### A Pluto.jl notebook ###
# v0.14.8

using Markdown
using InteractiveUtils

# ╔═╡ 6fe96300-8564-4ebb-a81e-09f47b34da33
using Pkg

# ╔═╡ f2399a45-4ab7-45ad-896b-47315c9dc910
Pkg.activate("Project.toml")

# ╔═╡ f83008bf-e73b-4185-9d6c-e2dc5ed35c30
using LinearAlgebra

# ╔═╡ e0939791-5c81-4d1a-8fdf-f2f6baaf14b5
using PlutoUI

# ╔═╡ e79d18df-f0a2-402d-85fb-9f93ea9a0f0d
using SymbolicUtils

# ╔═╡ ac2f3afa-f2fa-4c44-ad4b-ca291bdadf21
using Symbolics

# ╔═╡ b48a8b1a-9ec9-4abb-b8ad-1ca12bb0d723
 # each players payoff 

begin 

	pA_up1 = 1
	pA_up2 = 0
	pA_down1 =0
	pA_down2 = 2
	
	
	
	pA_left1 = 2
	pA_left2 = 0
	pA_right1 =0
	pA_right2 = 1
end 

# ╔═╡ 681a8135-8b48-427d-bb80-f08a4f0aaae6

	#creating player A

playerA =[pA_up1 pA_up2; pA_down1 pA_down2]




# ╔═╡ 36c65878-b010-4ae9-8ae3-54255e442863


# ╔═╡ 4aad6314-667f-4c49-8484-d689de6b4253
playerB = [pA_left1 pA_left2; pA_right1 pA_right2] #creating player B

# ╔═╡ 60ba7769-80ee-48e6-af97-98e12e75759d


# ╔═╡ 566f29f3-c550-43b5-bf1e-a57d14f71cee


# ╔═╡ c2909c6c-eca8-4905-946a-636a780c386f


# ╔═╡ 450ba23f-1d44-4ea4-831c-316587422b7a
#function to calculate Expected utility of one player given the action of another player
	
function nash(Pf,Pf2,Pf3,Pf4) # Takes in payoffs 
@variables σ_up
	
	 Eu1=σ_up*(Pf) + ((1-σ_up)*(Pf2))
   	 Eu2= σ_up*(Pf3) + ((1-σ_up)*(Pf4))
	 eq = Eu1~Eu2
	
	 Utility_1 =Symbolics.solve_for([eq],[σ_up])	
	 Utility_1= convert(Array{Any,1},Utility_1)
	Utility_1 = getindex(Utility_1,1)
	 Utility_2 = (1-getindex(Utility_1,1))
	
	return  [Utility_1,Utility_2]
end 

# ╔═╡ d95a9816-4a3a-4dda-a55e-77860ad3b271


# ╔═╡ a23f8246-6b9a-491e-8416-19bc50b360dd
PlayerA_strt= nash(pA_left1,pA_left2,pA_right1,pA_right2)

#probability of equilibrium for player A
	

# ╔═╡ 7c27d89d-763d-429a-a819-34b5ba3aba5d
PlayerB_strt = nash(pA_up1,	pA_up2,pA_down1,pA_down2)

#probability of equilibrium for player B

# ╔═╡ bd8cb891-5297-4a81-b36f-1284b656ca35
function prob()
	arr= []	
	for k in PlayerA_strt     #function to calculate equilibrium for each player 
			
		for j in PlayerB_strt
				
		i= k*j
		
		println(push!(arr,i))
			
				
				
	end
	end 
return arr
end 

# ╔═╡ 3a0d2859-1e04-4446-abb2-7d9f7c11764b
probabilities =prob()

# ╔═╡ d1f7d046-f5dc-41fa-9ba9-b3a973a06d17
(2*0.1875)+(1*0.0625)+(0*0.5625)+(3*0.1875)

# ╔═╡ 13f9234e-d89c-4720-b813-213365ff51c0
p2pay= dot(playerB,probabilities) #player 2 payoff


# ╔═╡ d9b17bc4-a39f-490c-8db3-c96df8bb85d1
p1pay= dot(playerA,probabilities) 

# ╔═╡ 0b598820-ff3d-4035-95e7-f1c58fec3a2a
typeof(p2pay)

# ╔═╡ b4a099dc-096e-4dfc-abee-eace54394899


# ╔═╡ dbfa447f-43b7-46a9-a5cb-b701429184b1


# ╔═╡ 3854fb88-e403-4b40-bc47-6a6a373f18b6


# ╔═╡ b990c184-e502-439e-84f4-941435b2325a


# ╔═╡ Cell order:
# ╠═f83008bf-e73b-4185-9d6c-e2dc5ed35c30
# ╠═6fe96300-8564-4ebb-a81e-09f47b34da33
# ╠═e0939791-5c81-4d1a-8fdf-f2f6baaf14b5
# ╠═e79d18df-f0a2-402d-85fb-9f93ea9a0f0d
# ╠═ac2f3afa-f2fa-4c44-ad4b-ca291bdadf21
# ╠═f2399a45-4ab7-45ad-896b-47315c9dc910
# ╠═b48a8b1a-9ec9-4abb-b8ad-1ca12bb0d723
# ╠═681a8135-8b48-427d-bb80-f08a4f0aaae6
# ╠═36c65878-b010-4ae9-8ae3-54255e442863
# ╠═4aad6314-667f-4c49-8484-d689de6b4253
# ╠═60ba7769-80ee-48e6-af97-98e12e75759d
# ╠═566f29f3-c550-43b5-bf1e-a57d14f71cee
# ╠═c2909c6c-eca8-4905-946a-636a780c386f
# ╠═450ba23f-1d44-4ea4-831c-316587422b7a
# ╠═d95a9816-4a3a-4dda-a55e-77860ad3b271
# ╠═a23f8246-6b9a-491e-8416-19bc50b360dd
# ╠═7c27d89d-763d-429a-a819-34b5ba3aba5d
# ╠═bd8cb891-5297-4a81-b36f-1284b656ca35
# ╠═3a0d2859-1e04-4446-abb2-7d9f7c11764b
# ╠═d1f7d046-f5dc-41fa-9ba9-b3a973a06d17
# ╠═13f9234e-d89c-4720-b813-213365ff51c0
# ╠═d9b17bc4-a39f-490c-8db3-c96df8bb85d1
# ╠═0b598820-ff3d-4035-95e7-f1c58fec3a2a
# ╠═b4a099dc-096e-4dfc-abee-eace54394899
# ╠═dbfa447f-43b7-46a9-a5cb-b701429184b1
# ╠═3854fb88-e403-4b40-bc47-6a6a373f18b6
# ╠═b990c184-e502-439e-84f4-941435b2325a
