### A Pluto.jl notebook ###
# v0.14.8

using Markdown
using InteractiveUtils

# ╔═╡ 1b500680-d4c4-11eb-20cf-b35823fc53d4
using Pkg

# ╔═╡ 59103f4f-9b97-4ee9-86fa-769ae0060caf
Pkg.activate("Project.toml")


# ╔═╡ e7e872d1-94e7-4297-9aa6-766a15779cad
using LinearAlgebra

# ╔═╡ de514ef6-6471-4725-b0ed-620d8457a9e0
using Symbolics

# ╔═╡ e3d67ecf-d2ae-44e3-8611-1d51aea9441c
using PlutoUI

# ╔═╡ 6ca90d7f-2bab-4384-beb5-4e2f4d60247f
md"# Creating States and Actions"

# ╔═╡ 8170e5ff-e4fe-4913-9ed9-b6ce7e8b4d13
struct state
stateName::String
Reward::Float64
 
end 


# ╔═╡ 5d3fd4ee-ee58-4ddc-ba90-49497584f27d
struct Action
 actionName::String
probability::Float64

end 

# ╔═╡ 69200799-85ff-474b-b8a8-7c2430e6b5bd
 A = Matrix{Any}

# ╔═╡ 40e673ca-47b0-465e-9151-5bc68f560df3
struct action
	
	Transition::Array{Float64}
end 

# ╔═╡ cf7901fc-ab07-4b5f-96f4-eb9a5133fc21
@variables s1 s2 s3 s4 s5 s6

# ╔═╡ cfb8fd8a-5709-4624-ba9d-3b2999329181
left= action([ 0 0.8 0.2 0 0 0; 0 0 0 0.8 0.2 0; 0 0 0 0 0.8 0.2; 0 0 0 0 0 0; 0 0 0 0 0 0; 0 0 0 0 0 0]) #Transition matrix for action left  e.g left.Transition[1,2] is probaility of state 2 given one for action left

# ╔═╡ dce6198a-d4fd-4097-89c2-47d6f9c7a30e
right = action([ 0 0.2 0.8 0 0 0; 0 0 0 0.2 0.8 0; 0 0 0 0 0.2 0.8; 0 0 0 0 0 0; 0 0 0 0 0 0;0 0 0 0 0 0])

# ╔═╡ b381d2e7-bb8e-4847-b3fa-400570c2f9e6
S_1 = state("S1",0)

# ╔═╡ e908e8ff-2bc1-404b-ad29-f1b5105ba9a3
S_2 = state("S2",2)

# ╔═╡ 06d61121-5739-49d7-9aff-78e38a1033fe
S_3 =state("S3",1)

# ╔═╡ ac90ce43-86df-4829-9a86-ead07f4ce4dd
S_4 = state("S4",-2)

# ╔═╡ 75d26b0a-840a-450f-b511-6caee6fa5468
S_5= state("S5",1)

# ╔═╡ d08b1e71-c68d-4ae1-b3cd-484fb9bf82a5
S_6 = state("S6",-0.5)

# ╔═╡ bc25134d-f6ae-4cff-ae85-0914b4034f53
stateSpace=[S_1,S_2,S_3,S_4,S_5,S_6]

# ╔═╡ f041498a-f101-441f-a512-2b81b59c885c
	

# ╔═╡ 51213f0e-774b-4a57-a15f-0715378c292e


# ╔═╡ d61f8a33-aedc-4ee0-946f-75936044207c
md" # Creating transition Model"

# ╔═╡ 0ad8ed8b-fa1e-40eb-8832-4da21e6b4804
left.Transition

# ╔═╡ b9e0b931-ec4a-4fcb-b759-d8fcaa2e8c2f
right.Transition

# ╔═╡ a1a3c154-8da2-412a-8a94-a7f088bab252

 function Transget(array)


store=[]
for  i =1:6  j= 1:6
	
	println(push!(store,array.Transition[i,j]))
        end 
	return store
	end 		

	

# ╔═╡ eb6e6e17-6601-4b5d-b258-1865bdde43f9
left1= Transget(left)

# ╔═╡ 3ff47e4e-797e-4b7a-a700-b927d3825ac5
function Initial(states)#our initial utility will be zero for all states 
	Vs=[]
	for i in states
		println(push!(Vs,i.Reward))
		 
		
	end 
 
	return Vs
end 
		

# ╔═╡ 31feee0e-7c51-4530-89eb-fdb3de19d20f
V= Initial(stateSpace)

# ╔═╡ b83134b1-4a81-4233-9103-6b29ecf92d20
right1= Transget(right)

# ╔═╡ a02394c2-9319-4622-b210-79627ce7e097
getindex(right1[1],2)

# ╔═╡ 8664ba58-56c6-4e90-8c48-d6931d15098d
right1[3]

# ╔═╡ 6cce8685-16a3-4d3a-b635-f2a0f3d85425
g=getindex(right1[1],2)

# ╔═╡ e225065d-ed1f-43f1-b36d-1f84cb25b36c
typeof(right1)

# ╔═╡ 87bbada5-bf13-4cfd-8afa-5ad049430c56
left1[3]

# ╔═╡ cade2fc6-bc62-4901-9fc1-e499404d236a


# ╔═╡ abbad9db-47d1-4b00-958d-e5b1bc1799a6
function improve()
	
vs1 = (getindex(left1[1],2)*V[2])+(getindex(left1[1],3)*V[3])
vs2 = (getindex(right1[1],2)*V[2])+(getindex(right1[1],3)*V[3])# calcuating new utilities and improving 
	
vs3=(getindex(left1[2],4)*V[4])+(getindex(left1[2],5)*V[5])
vs4=(getindex(right1[2],4)*V[5])+(getindex(right1[2],4)*V[5])
	
vs5=(getindex(left1[3],5)*V[5])+(getindex(left1[3],6)*V[6])
vs6=(getindex(right1[3],5)*V[5])+(getindex(right1[3],6)*V[6])
	
	return [vs1 vs2 vs3 vs4 vs5 vs6]
end 


# ╔═╡ 4a6a9df2-73df-445f-971e-3d96cb4c7f58
newV=improve()

# ╔═╡ 26c40578-0ac3-4145-8856-11c01f21f521
@variables L R

# ╔═╡ 81f3fe41-a029-42df-b28c-10969979a685
policy =[L L L ]

# ╔═╡ 90e8ac72-e53b-485e-a52f-2eb8d4147052
begin 
vs1 = newV[1]+ newV[2]+ S_1.Reward
vs2 = newV[3]+ newV[4]+S_2.Reward
vs3 =newV[5]+ newV[6]+S_3.Reward
end 

# ╔═╡ 1c91b7e0-14f2-4f73-b6fe-627673b99c6e
V2=[vs1 vs2 vs3 newV[4] newV[5] newV[6]] #new iteration 

# ╔═╡ 2dab6403-42e0-4e08-881f-e60e1b51e74c


# ╔═╡ 1e382bb4-d3ef-4ed5-ad16-91ccb786daf2
function improve2()
vs1 = (getindex(left1[1],2)*V2[2])+(getindex(left1[1],3)*V2[3])
vs2 = (getindex(right1[1],2)*V2[2])+(getindex(right1[1],3)*V2[3])
	
vs3=(getindex(left1[2],4)*V2[5])+(getindex(left1[2],5)*V2[5])
vs4=(getindex(right1[2],4)*V2[5])+(getindex(right1[2],4)*V2[5])
	
vs5=(getindex(left1[3],5)*V2[5])+(getindex(left1[3],6)*V2[6])
vs6=(getindex(right1[3],5)*V2[5])+(getindex(right1[3],6)*V2[6])
	return [vs1 vs2 vs3 vs4 vs5 vs6]
end 

# ╔═╡ 6344f8d6-a4ed-4f75-afe8-3fd6a3eb4c26
#=vs = getindex(left1[1],2)*V[2]+ getindex(left1[1])
vs2 = getindex(right1[1],2)*V[3]+getindex(right1[2])=#

# ╔═╡ d1bfccaa-a9ac-473b-8702-dca59c88aeb0
improve2

# ╔═╡ 7773bfb6-c86a-4531-8c26-c0d197b564b0


# ╔═╡ d0deed06-cc72-4ffd-94c9-d3943119595f


function my()
		s=[]
	for j =1:6
	for i in left1[j]

	if i >0 
			
		println(push!(s,i))
	end
		
		
end
	end
	return s
end 

# ╔═╡ 3e98975a-cba2-4877-8704-aa91c73eb6ae


# ╔═╡ abcfdfa8-9372-43fb-84df-f6efbc7edf40
#=function my()
		s=[]
	for i in left.Transition

	if i >0
		println(push!(s,i))
	end
		
		
end 
	return s
end =#

# ╔═╡ f64cf6b5-8aa2-44e3-b91b-a1de59f34f4e
md"# in case needed"

# ╔═╡ d9053852-a831-4f1b-b37c-6ccb0c289333
#deleteat! removing from array 

# ╔═╡ c3518f3e-5e41-4297-bc9c-b12a3eb50534


# ╔═╡ Cell order:
# ╠═1b500680-d4c4-11eb-20cf-b35823fc53d4
# ╠═e7e872d1-94e7-4297-9aa6-766a15779cad
# ╠═de514ef6-6471-4725-b0ed-620d8457a9e0
# ╠═e3d67ecf-d2ae-44e3-8611-1d51aea9441c
# ╠═59103f4f-9b97-4ee9-86fa-769ae0060caf
# ╠═6ca90d7f-2bab-4384-beb5-4e2f4d60247f
# ╠═8170e5ff-e4fe-4913-9ed9-b6ce7e8b4d13
# ╠═5d3fd4ee-ee58-4ddc-ba90-49497584f27d
# ╠═69200799-85ff-474b-b8a8-7c2430e6b5bd
# ╠═40e673ca-47b0-465e-9151-5bc68f560df3
# ╠═cf7901fc-ab07-4b5f-96f4-eb9a5133fc21
# ╠═cfb8fd8a-5709-4624-ba9d-3b2999329181
# ╠═dce6198a-d4fd-4097-89c2-47d6f9c7a30e
# ╠═b381d2e7-bb8e-4847-b3fa-400570c2f9e6
# ╠═e908e8ff-2bc1-404b-ad29-f1b5105ba9a3
# ╠═06d61121-5739-49d7-9aff-78e38a1033fe
# ╠═ac90ce43-86df-4829-9a86-ead07f4ce4dd
# ╠═75d26b0a-840a-450f-b511-6caee6fa5468
# ╠═d08b1e71-c68d-4ae1-b3cd-484fb9bf82a5
# ╠═bc25134d-f6ae-4cff-ae85-0914b4034f53
# ╠═f041498a-f101-441f-a512-2b81b59c885c
# ╠═51213f0e-774b-4a57-a15f-0715378c292e
# ╠═d61f8a33-aedc-4ee0-946f-75936044207c
# ╠═0ad8ed8b-fa1e-40eb-8832-4da21e6b4804
# ╠═b9e0b931-ec4a-4fcb-b759-d8fcaa2e8c2f
# ╠═a1a3c154-8da2-412a-8a94-a7f088bab252
# ╠═eb6e6e17-6601-4b5d-b258-1865bdde43f9
# ╠═a02394c2-9319-4622-b210-79627ce7e097
# ╠═3ff47e4e-797e-4b7a-a700-b927d3825ac5
# ╠═31feee0e-7c51-4530-89eb-fdb3de19d20f
# ╠═b83134b1-4a81-4233-9103-6b29ecf92d20
# ╠═8664ba58-56c6-4e90-8c48-d6931d15098d
# ╠═6cce8685-16a3-4d3a-b635-f2a0f3d85425
# ╠═e225065d-ed1f-43f1-b36d-1f84cb25b36c
# ╠═87bbada5-bf13-4cfd-8afa-5ad049430c56
# ╠═cade2fc6-bc62-4901-9fc1-e499404d236a
# ╠═abbad9db-47d1-4b00-958d-e5b1bc1799a6
# ╠═4a6a9df2-73df-445f-971e-3d96cb4c7f58
# ╠═26c40578-0ac3-4145-8856-11c01f21f521
# ╠═81f3fe41-a029-42df-b28c-10969979a685
# ╠═90e8ac72-e53b-485e-a52f-2eb8d4147052
# ╠═1c91b7e0-14f2-4f73-b6fe-627673b99c6e
# ╠═2dab6403-42e0-4e08-881f-e60e1b51e74c
# ╠═1e382bb4-d3ef-4ed5-ad16-91ccb786daf2
# ╠═6344f8d6-a4ed-4f75-afe8-3fd6a3eb4c26
# ╠═d1bfccaa-a9ac-473b-8702-dca59c88aeb0
# ╠═7773bfb6-c86a-4531-8c26-c0d197b564b0
# ╠═d0deed06-cc72-4ffd-94c9-d3943119595f
# ╠═3e98975a-cba2-4877-8704-aa91c73eb6ae
# ╠═abcfdfa8-9372-43fb-84df-f6efbc7edf40
# ╠═f64cf6b5-8aa2-44e3-b91b-a1de59f34f4e
# ╠═d9053852-a831-4f1b-b37c-6ccb0c289333
# ╠═c3518f3e-5e41-4297-bc9c-b12a3eb50534
